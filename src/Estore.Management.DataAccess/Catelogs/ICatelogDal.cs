﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICatelogDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandTypeId"></param>
        /// <returns></returns>
        Task<List<CatelogInfoDto>> FetchList(int? brandTypeId = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        Task<List<SelectedDateCatelogInfoDto>> FetchList(DateTime dateTime, decimal catelogId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        Task<CatelogDto> Get(decimal catelogId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(CatelogDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(CatelogDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        Task UpdateStatus(decimal catelogId);
    }
}
