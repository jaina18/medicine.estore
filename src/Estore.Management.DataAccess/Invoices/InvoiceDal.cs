﻿using Estore.Management.DataAccess.InvoiceLines;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceDal : IInvoiceDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<InvoiceInfoDto> Get(decimal invoiceId)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var dto = (from c in ctx.Invoices
                           where c.InvoiceId == invoiceId
                           select new InvoiceInfoDto()
                           {
                               Amount = c.Amount,
                               Tax = c.Tax ?? 0,
                               InvoiceDate = c.InvoiceDate,
                               InvoiceId = c.InvoiceId,
                               InvoiceNumber = c.InvoiceNumber,
                               RetailerId = c.Retailer.RetailerId,
                               Lines = c.InvoiceLines.Select(line => new InvoiceLineInfoDto()
                               {
                                   CatelogId = line.CatelogId,
                                   BrandTypeId = line.Catelog.BrandTypeId,
                                   CatelogName = line.Catelog.Name + " ( " + line.Catelog.GSTPercentage.ToString() + " )",
                                   LineId = line.LineId,
                                   LineNumber = line.LineNumber,
                                   Price = line.Price,
                                   Tax = line.Tax ?? 0,
                                   Quantity = line.Quantity
                               }).ToList()
                           }).SingleOrDefault();
                return dto;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(InvoiceDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = new Invoice()
                {
                    Amount = dto.Amount,
                    Archived = dto.Archived,
                    InvoiceDate = dto.InvoiceDate,
                    InvoiceNumber = dto.InvoiceNumber,
                    RetailerId = dto.RetailerId,
                    Tax = dto.Tax
                };

                ctx.Invoices.Add(data);
                await ctx.SaveChangesAsync();

                dto.InvoiceId = data.InvoiceId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(InvoiceDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Invoices
                            where c.InvoiceId == dto.InvoiceId
                            select c).SingleOrDefault();

                var invoiceLines = (from c in ctx.InvoiceLines
                                    where c.InvoiceId == dto.InvoiceId
                                    select c).ToList();

                invoiceLines.ForEach(item =>
                {
                    ctx.InvoiceLines.Remove(item);
                });

                ctx.Invoices.Remove(data);
                await ctx.SaveChangesAsync();
            }
        }
    }
}
