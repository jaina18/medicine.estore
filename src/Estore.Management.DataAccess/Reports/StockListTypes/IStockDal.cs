﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStockDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="brandTypeId"></param>
        /// <returns></returns>
        Task<List<StockDto>> FetchList(int year, int month, int? brandTypeId = null);
    }
}
