﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StockCatelogDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int BrandTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? PurchasedQuantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SaleQuantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Quantity 
        { 
            get 
            {
                var purchasedQuauntity = PurchasedQuantity != null ? PurchasedQuantity : 0;
                var saleQuantity = SaleQuantity != null ? SaleQuantity : 0;
                return (int)(purchasedQuauntity - (saleQuantity));
            } 
        }
    }
}
