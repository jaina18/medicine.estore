﻿using System;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DailyRecieptDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RecieptId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RetailerName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string RecieptNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
    }
}
