﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDailyRecieptDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        Task<List<DailyRecieptDto>> FetchList(DateTime date);
    }
}
