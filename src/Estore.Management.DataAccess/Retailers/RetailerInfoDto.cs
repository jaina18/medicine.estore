﻿using Estore.Management.DataAccess.Cities;
using System;
using System.Collections.Generic;

namespace Estore.Management.DataAccess.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RetailerInfoDto
    {
        /// <summary>
        /// 
        /// </summary>
        public RetailerInfoDto()
        {
            this.Transactions = new List<TransactionDto>();
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string City { get { return CityDal.GetName(CityId); } }
        
        /// <summary>
        /// 
        /// </summary>
        public string MobileNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal BilledAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CollectedAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<TransactionDto> Transactions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastRecieptDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LastRecieptAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastInvoiceDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LastInvoiceAmount { get; set; }
    }
}
