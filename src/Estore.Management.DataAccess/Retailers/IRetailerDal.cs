﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRetailerDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="queryExpression"></param>
        /// <param name="archived"></param>
        /// <returns></returns>
        Task<List<RetailerInfoDto>> FetchList(int cityId, string queryExpression, bool? archived = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        Task<RetailerDto> Fetch(decimal retailerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        Task<RetailerInfoDto> FetchInfo(decimal retailerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(RetailerDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(RetailerDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        Task Delete(decimal retailerId);
    }
}
