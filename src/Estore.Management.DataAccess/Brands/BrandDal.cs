﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Brands
{
    public static class BrandDal
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly Dictionary<int, string> BrandList;

        /// <summary>
        /// 
        /// </summary>
        static BrandDal()
        {
            if (BrandList == null)
            {
                BrandList = new Dictionary<int, string>
                {
                    { 1, "Polymed" },
                    { 2, "Other" }
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetName(int brandId)
        {
            string name;
            BrandList.TryGetValue(brandId, out name);
            return name ?? "Other";
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<BrandDto> Get()
        {
            var dtos = new List<BrandDto>();
            foreach (var item in BrandList)
            {
                dtos.Add(new BrandDto(item.Key, item.Value));
            }
            return dtos.OrderBy(x => x.Name).ToList();
        }
    }
}
