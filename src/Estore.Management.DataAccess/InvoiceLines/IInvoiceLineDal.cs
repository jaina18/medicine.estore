﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.InvoiceLines
{
    /// <summary>
    /// 
    /// </summary>
    public interface IInvoiceLineDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(InvoiceLineDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Delete(InvoiceLineDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        Task<List<InvoiceLineDto>> FetchList(decimal invoiceId);
    }
}
