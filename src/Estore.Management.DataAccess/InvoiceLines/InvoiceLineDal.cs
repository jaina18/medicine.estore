﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.InvoiceLines
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceLineDal : IInvoiceLineDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<List<InvoiceLineDto>> FetchList(decimal invoiceId)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.InvoiceLines
                            where c.InvoiceId == invoiceId
                            select new InvoiceLineDto()
                            {
                                Archived = c.Archived,
                                CatelogId = c.CatelogId,
                                InvoiceId = c.InvoiceId,
                                LineId = c.LineId,
                                LineNumber = c.LineNumber,
                                Price = c.Price,
                                Tax = c.Tax ?? 0,
                                Quantity = c.Quantity
                            }).ToList();

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(InvoiceLineDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = new InvoiceLine()
                {
                    Archived = dto.Archived,
                    CatelogId = dto.CatelogId,
                    InvoiceId = dto.InvoiceId,
                    LineId = dto.LineId,
                    LineNumber = dto.LineNumber,
                    Price = dto.Price,
                    Quantity = dto.Quantity,
                    Tax = dto.Tax
                };

                ctx.InvoiceLines.Add(data);
                await ctx.SaveChangesAsync();

                dto.LineId = data.LineId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Delete(InvoiceLineDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.InvoiceLines
                            where c.LineId == dto.LineId
                            select c).SingleOrDefault();

                ctx.InvoiceLines.Remove(data);
                await ctx.SaveChangesAsync();
            }
        }
    }
}
