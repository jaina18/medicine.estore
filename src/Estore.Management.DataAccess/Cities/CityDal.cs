﻿using System.Linq;
using System.Collections.Generic;

namespace Estore.Management.DataAccess.Cities
{
    public static class CityDal
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly Dictionary<int, string> CityList;

        /// <summary>
        /// 
        /// </summary>
        static CityDal()
        {
            if (CityList == null)
            {
                CityList = new Dictionary<int, string>
                {
                    { 1, "Bhopal" },
                    { 2, "Indore" },
                    { 3, "Gwalior" },
                    { 4, "Jabalpur" },
                    { 5, "Bhind" },
                    { 6, "Hosangabaad" },
                    { 7, "Other" }
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetName(int cityId)
        {
            string cityName;
            CityList.TryGetValue(cityId, out cityName);
            return cityName ?? "Other";
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<CityDto> Get()
        {
            var dtos = new List<CityDto>();
            foreach(var item in CityList)
            {
                dtos.Add(new CityDto(item.Key, item.Value));
            }
            return dtos.OrderBy(x => x.Name).ToList();
        }
    }
}
