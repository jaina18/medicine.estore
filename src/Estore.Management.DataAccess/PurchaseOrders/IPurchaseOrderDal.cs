﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPurchaseOrderDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(PurchaseOrderDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        Task Remove(decimal purchaseOrderId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        Task<List<PurchaseOrderDto>> FetchList(decimal catelogId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="poDate"></param>
        Task<List<PurchaseOrderDto>> FetchList(DateTime poDate);
    }
}
