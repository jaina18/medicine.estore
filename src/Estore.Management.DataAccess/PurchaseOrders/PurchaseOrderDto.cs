﻿using System;

namespace Estore.Management.DataAccess.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PurchaseOrderDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal PurchaseOrderId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int BrandTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CatelogDisplayName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Tax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double TaxAmount 
        { 
            get 
            {
                return Math.Round((double)(Tax * Price * Quantity) / 100); 
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}
