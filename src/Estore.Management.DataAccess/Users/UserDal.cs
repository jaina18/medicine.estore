﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Users
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class UserDal : IUserDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(string emailAddress, string password)
        {
            await Task.FromResult(1);
            using(var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Users
                           where c.Active == true && c.EmailAddress == emailAddress
                           && c.Password == password
                           select c).SingleOrDefault();

                return new UserDto()
                {
                    Active = data.Active,
                    EmailAddress = data.EmailAddress,
                    EmailVerified = data.EmailVerified,
                    Id = data.Id,
                    LastLogin = data.LastLogin,
                    Name = data.Name,
                    Password = data.Password
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public void Insert(UserDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var newItem = new User()
                {
                    Active = dto.Active,
                    EmailAddress = dto.EmailAddress,
                    EmailVerified = dto.EmailVerified,
                    Id = dto.Id,
                    LastLogin = dto.LastLogin,
                    Name = dto.Name,
                    Password = dto.Password
                };

                ctx.Users.Add(newItem);
                ctx.SaveChanges();
            }
        }
    }
}
