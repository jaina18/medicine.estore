﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reciepts
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RecieptDal : IRecieptDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(RecieptDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = new Reciept()
                {
                    Amount = dto.Amount,
                    Archived = dto.Archived,
                    RecieptDate = dto.RecieptDate,
                    RecieptNumber = dto.RecieptNumber,
                    RetailerId = dto.RetailerId
               };

                ctx.Reciepts.Add(data);
                await ctx.SaveChangesAsync();

                dto.RecieptId = data.RecieptId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(RecieptDto dto)
        {
            using(var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Reciepts
                            where c.RecieptId == dto.RecieptId
                            select c).SingleOrDefault();

                ctx.Reciepts.Remove(data);
                await ctx.SaveChangesAsync();
            }
        }
    }
}
