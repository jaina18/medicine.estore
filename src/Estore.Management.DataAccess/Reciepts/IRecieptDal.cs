﻿using System;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reciepts
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRecieptDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(RecieptDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(RecieptDto dto);
    }
}
