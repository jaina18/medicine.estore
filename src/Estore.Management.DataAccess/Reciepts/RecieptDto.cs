﻿using System;

namespace Estore.Management.DataAccess.Reciepts
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RecieptDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RecieptId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string RecieptNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime RecieptDate { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }
    }
}
