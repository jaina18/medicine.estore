﻿using Estore.Management.Library.Catelogs;
using Estore.Management.Library.PurchaseOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class DailyPurchaseReportController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IPurchaseOrder purchaseOrder;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelogList catelogList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPurchaseOrderList purchaseOrderList;

        /// <summary>
        /// 
        /// </summary>
        public DailyPurchaseReportController()
        {
            this.purchaseOrder = new PurchaseOrder();
            this.purchaseOrderList = new PurchaseOrderList();
            this.catelogList = new CatelogList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="poDate"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(DateTime poDate)
        {
            ViewBag.CatelogList = await catelogList.Get();
            ViewBag.SelectedDate = poDate;
            var model = await purchaseOrderList.Get(poDate);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        /// <param name="poDate"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Remove(decimal purchaseOrderId, DateTime poDate)
        {
            ViewBag.SelectedDate = poDate;
            await purchaseOrder.Remove(purchaseOrderId);
            return RedirectToAction("Index", new { @poDate = poDate.ToString("yyyy-MM-dd") });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> New(PurchaseOrderDao dao)
        {
            await purchaseOrder.New(dao);
            return RedirectToAction("Index", new { @poDate = dao.OrderDate.ToString("yyyy-MM-dd") });
        }
    }
}