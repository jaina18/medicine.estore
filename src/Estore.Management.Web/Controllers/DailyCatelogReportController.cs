﻿using Estore.Management.Library.Reports;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class DailyCatelogReportController : Controller
    {

        /// <summary>
        /// 
        /// </summary>
        private readonly IReport<List<DailyCatelogSaleDao>, DailyCatelogSaleCriteria> report;

        /// <summary>
        /// 
        /// </summary>
        public DailyCatelogReportController()
        {
            this.report = new DailyCatelogSale();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(string date)
        {
            DateTime dateTime;
            if (string.IsNullOrEmpty(date))
            {
                dateTime = DateTime.Now.Date;
            }
            else
            {
                dateTime = DateTime.ParseExact(date, "yyyy-MM-dd", null);
            }
            ViewBag.SelectedDate = dateTime;
            var list = await report.Get(new DailyCatelogSaleCriteria(dateTime));
            return View(list);
        }
    }
}