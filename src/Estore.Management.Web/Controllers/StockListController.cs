﻿using System;
using System.Collections.Generic;
using Estore.Management.Library.Reports;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class StockListController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IReport<List<StockDao>, StockCriteria> report;

        /// <summary>
        /// 
        /// </summary>
        public StockListController()
        {
            this.report = new StockReport();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandTypeId"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(int? month = null, int? brandTypeId = null)
        {
            ViewBag.SelectedMonthId = month;
            ViewBag.SelectedBrandTypeId = brandTypeId;
            ViewBag.SelectedMonth = await GetMonth(month);
            var criteria = new StockCriteria()
            {
                Year = DateTime.Now.Year,
                Month = month != null ? month.Value : DateTime.Now.Month,
                BrandTypeId = brandTypeId
            };
            var model = await report.Get(criteria);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        private async Task<string> GetMonth(int? month = null)
        {
            await Task.FromResult(1);
            var monthName = string.Empty;
            switch(month)
            {
                case 1:
                    monthName = "January";
                    break;
                case 2:
                    monthName = "February";
                    break;
                case 3:
                    monthName = "March";
                    break;
                case 4:
                    monthName = "Apirl";
                    break;
                case 5:
                    monthName = "May";
                    break;
                case 6:
                    monthName = "June";
                    break;
                case 7:
                    monthName = "July";
                    break;
                case 8:
                    monthName = "August";
                    break;
                case 9:
                    monthName = "September";
                    break;
                case 10:
                    monthName = "Octuber";
                    break;
                case 11:
                    monthName = "November";
                    break;
                case 12:
                    monthName = "December";
                    break;
            }
            return monthName;
        }
    }
}