﻿using Estore.Management.Library.Retailers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class RetailerListController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRetailerList RetailerList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRetailer Retailer;

        /// <summary>
        /// 
        /// </summary>
        public RetailerListController()
        {
            this.Retailer = new Retailer();
            this.RetailerList = new RetailerList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="queryExpression"></param>
        /// <param name="archived"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(int cityId = 0, string queryExpression = null, bool? archived = null)
        {
            ViewBag.Archived = archived.HasValue && archived.Value;
            ViewBag.SelectedCityId = cityId;
            ViewBag.SelectedQueryExpression = queryExpression;
            var list = await RetailerList.Get(cityId, queryExpression, archived);
            return View(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="queryExpression"></param>
        /// <param name="archived"></param>
        /// <returns></returns>
        public async Task<ActionResult> Export(int cityId = 0, string queryExpression = null, bool? archived = null)
        {
            ViewBag.Archived = archived.HasValue && archived.Value;
            ViewBag.SelectedCityId = cityId;
            ViewBag.SelectedQueryExpression = queryExpression;
            var list = await RetailerList.Get(cityId, queryExpression, archived);
            return View(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Save(RetailerDao dao)
        {
            if (dao.RetailerId == 0)
            {
                await Retailer.New(dao);
                if (dao.Status)
                {
                    return RedirectToAction("Index", "RetailerList", new { @cityId = dao.CityId });
                }
                return View(dao);
            }
            else
            {
                await Retailer.Save(dao);
                if (dao.Status)
                {
                    return RedirectToAction("Index", "RetailerList", new { @CityId = dao.CityId });
                }
                return View(dao);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<JsonResult> Get(decimal retailerId)
        {
            var model = await Retailer.Get(retailerId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}