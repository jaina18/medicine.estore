﻿using Estore.Management.Library.PurchaseOrders;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class PurchaseOrderListController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IPurchaseOrder purchaseOrder;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPurchaseOrderList purchaseOrderList;

        /// <summary>
        /// 
        /// </summary>
        public PurchaseOrderListController()
        {
            this.purchaseOrder = new PurchaseOrder();
            this.purchaseOrderList = new PurchaseOrderList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal catelogId, string displayName)
        {
            ViewBag.CatelogName = displayName;
            ViewBag.CatelogId = catelogId;
            var model = await purchaseOrderList.Get(catelogId);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> New(PurchaseOrderDao dao)
        {
            ViewBag.CatelogId = dao.CatelogId;
            await purchaseOrder.New(dao);
            return RedirectToAction("Index", new { @catelogId = dao.CatelogId, @displayName = dao.CatelogDisplayName });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Remove(decimal purchaseOrderId, decimal catelogId, string displayName)
        {
            ViewBag.CatelogId = catelogId;
            await purchaseOrder.Remove(purchaseOrderId);
            return RedirectToAction("Index", new { @catelogId = catelogId, @displayName = displayName });
        }
    }
}