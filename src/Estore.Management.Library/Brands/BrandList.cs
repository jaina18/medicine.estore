﻿using Estore.Management.DataAccess.Brands;
using System.Collections.Generic;

namespace Estore.Management.Library.Brands
{
    public sealed class BrandList
    {
        /// <summary>
        /// 
        /// </summary>
        public BrandList()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<BrandDao> Get()
        {
            var daos = new List<BrandDao>();
            foreach(var item in BrandDal.Get())
            {
                daos.Add(new BrandDao(item.Id, item.Name));
            }
            return daos;
        }
    }
}
