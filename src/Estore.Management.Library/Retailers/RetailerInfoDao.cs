﻿using Estore.Management.Library.Shared;
using System;

namespace Estore.Management.Library.Retailers
{
    public sealed class RetailerInfoDao : EditDao
    {
        /// <summary>
        /// 
        /// </summary>
        public RetailerInfoDao()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MobileNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal BilledAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CollectedAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Balance { get { return BilledAmount - CollectedAmount; } }

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.Generic.List<TransactionDao> Transactions {get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastRecieptDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LastRecieptAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastInvoiceDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LastInvoiceAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get { return this.Name + " " + this.City; } }
    }
}
