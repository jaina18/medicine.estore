﻿using Estore.Management.DataAccess.Retailers;
using System.Threading.Tasks;
using System.Linq;

namespace Estore.Management.Library.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Retailer : IRetailer
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRetailerDal dal;

        /// <summary>
        /// 
        /// </summary>
        public Retailer()
        {
            this.dal = new RetailerDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Save(RetailerDao dao)
        {
            if (string.IsNullOrEmpty(dao.Name) || dao.CityId == 0 || string.IsNullOrEmpty(dao.MobileNumber))
            {
                dao.Status = false;
                dao.ErrorMessage = "Please fill all mandatory fields.";
                return;
            }
            var dto = new RetailerDto()
            {
                Active = dao.Active,
                Address = dao.Address,
                Archived = dao.Archived,
                CityId = dao.CityId,
                MobileNumber = dao.MobileNumber,
                Name = dao.Name,
                NickName = dao.NickName,
                RetailerId = dao.RetailerId
            };
            await dal.Update(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(RetailerDao dao)
        {
            if (string.IsNullOrEmpty(dao.Name) || dao.CityId == 0 || string.IsNullOrEmpty(dao.MobileNumber))
            {
                dao.Status = false;
                dao.ErrorMessage = "Please fill all mandatory fields.";
                return;
            }
            var dto = new RetailerDto()
            {
                Active = dao.Active,
                Address = dao.Address,
                Archived = dao.Archived,
                CityId = dao.CityId,
                MobileNumber = dao.MobileNumber,
                Name = dao.Name,
                NickName = dao.NickName,
                RetailerId = dao.RetailerId
            };
            await dal.Insert(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        public async Task<RetailerDao> Get(decimal retailerId)
        {
            var dto = await dal.Fetch(retailerId);
            return new RetailerDao()
            {
                Active = dto.Active,
                Address = dto.Address,
                Archived = dto.Archived,
                CityId = dto.CityId,
                MobileNumber = dto.MobileNumber,
                Name = dto.Name,
                NickName = dto.NickName,
                RetailerId = dto.RetailerId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        public async Task<RetailerInfoDao> GetInfo(decimal retailerId)
        {
            var dto = await dal.FetchInfo(retailerId);
            return new RetailerInfoDao()
            {
                Address = dto.Address,
                City = dto.City,
                MobileNumber = dto.MobileNumber,
                Name = dto.Name,
                NickName = dto.NickName,
                RetailerId = dto.RetailerId,
                CollectedAmount = dto.CollectedAmount,
                BilledAmount = dto.BilledAmount,
                Transactions = dto.Transactions.Select(m => new TransactionDao()
                { 
                    Amount = m.Amount,
                    BillNumber = m.BillNumber,
                    RetailerId = m.RetailerId,
                    TransactionDate = m.TransactionDate,
                    TransactionId = m.TransactionId,
                    TransactionType = m.TransactionType
                }).OrderBy(x => x.TransactionDate).ToList()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Delete(RetailerDao dao)
        {
            var dto = new RetailerDto()
            {
                Active = dao.Active,
                Address = dao.Address,
                Archived = true,
                CityId = dao.CityId,
                MobileNumber = dao.MobileNumber,
                Name = dao.Name,
                NickName = dao.NickName,
                RetailerId = dao.RetailerId
            };
            await dal.Update(dto);
        }
    }
}
