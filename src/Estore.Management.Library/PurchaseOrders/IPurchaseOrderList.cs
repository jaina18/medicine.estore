﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.Library.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPurchaseOrderList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        Task<List<PurchaseOrderDao>> Get(decimal catelogId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="poDate"></param>
        /// <returns></returns>
        Task<List<PurchaseOrderDao>> Get(DateTime poDate);
    }
}
