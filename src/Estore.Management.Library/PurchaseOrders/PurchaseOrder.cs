﻿using Estore.Management.DataAccess.PurchaseOrders;
using System;
using System.Threading.Tasks;

namespace Estore.Management.Library.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PurchaseOrder : IPurchaseOrder
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IPurchaseOrderDal dal;

        /// <summary>
        /// 
        /// </summary>
        public PurchaseOrder()
        {
            this.dal = new PurchaseOrderDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaserOrderId"></param>
        /// <returns></returns>
        public async Task<PurchaseOrderDao> Get(decimal purchaserOrderId)
        {
            await Task.FromResult(1);
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(PurchaseOrderDao dao)
        {
            var dto = new PurchaseOrderDto()
            {
                Archived = dao.Archived,
                CatelogId = dao.CatelogId,
                OrderDate = dao.OrderDate,
                Price = dao.Price,
                PurchaseOrderId = dao.PurchaseOrderId,
                Quantity = dao.Quantity,
                Tax = dao.TaxPercentage
            };
            await dal.Insert(dto);
            dao.PurchaseOrderId = dto.PurchaseOrderId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        /// <returns></returns>
        public async Task Remove(decimal purchaseOrderId)
        {
            await dal.Remove(purchaseOrderId);
        }
    }
}
