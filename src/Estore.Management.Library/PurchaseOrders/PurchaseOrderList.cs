﻿using Estore.Management.DataAccess.PurchaseOrders;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PurchaseOrderList : IPurchaseOrderList
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IPurchaseOrderDal purchaseOrderDal;

        /// <summary>
        /// 
        /// </summary>
        public PurchaseOrderList()
        {
            this.purchaseOrderDal = new PurchaseOrderDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task<List<PurchaseOrderDao>> Get(decimal catelogId)
        {
            var daos = new List<PurchaseOrderDao>();
            var dtos = await purchaseOrderDal.FetchList(catelogId);
            dtos.ForEach(item => daos.Add(new PurchaseOrderDao()
            {
                Archived = item.Archived,
                CatelogId = item.CatelogId,
                OrderDate = item.OrderDate,
                Price = item.Price,
                PurchaseOrderId = item.PurchaseOrderId,
                Quantity = item.Quantity,
                TaxPercentage = item.Tax,
                TaxAmount = item.TaxAmount
            }));
            return daos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="poDate"></param>
        /// <returns></returns>
        public async Task<List<PurchaseOrderDao>> Get(DateTime poDate)
        {
            var daos = new List<PurchaseOrderDao>();
            var dtos = await purchaseOrderDal.FetchList(poDate);
            dtos.ForEach(item => daos.Add(new PurchaseOrderDao()
            {
                Archived = item.Archived,
                CatelogId = item.CatelogId,
                BrandName = DataAccess.Brands.BrandDal.GetName(item.BrandTypeId),
                CatelogDisplayName = item.CatelogDisplayName,
                OrderDate = item.OrderDate,
                Price = item.Price,
                PurchaseOrderId = item.PurchaseOrderId,
                Quantity = item.Quantity,
                TaxPercentage = item.Tax,
                TaxAmount = item.TaxAmount
            }));
            return daos;
        }
    }
}
