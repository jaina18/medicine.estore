﻿using Estore.Management.DataAccess.Reciepts;
using System.Threading.Tasks;

namespace Estore.Management.Library.Reciepts
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Reciept : IReciept
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRecieptDal dal;

        /// <summary>
        /// 
        /// </summary>
        public Reciept()
        {
            this.dal = new RecieptDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Delete(RecieptDao dao)
        {
            var dto = new RecieptDto()
            {
                RecieptId = dao.RecieptId,
                Archived = true
            };
            await dal.Update(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(RecieptDao dao)
        {
            if (string.IsNullOrEmpty(dao.RecieptNumber) || dao.Amount == 0)
            {
                dao.Status = false;
                dao.ErrorMessage = "Please fill all mandatory fields.";
                return;
            }
            var dto = new RecieptDto()
            {
                Amount = dao.Amount,
                RecieptNumber = dao.RecieptNumber,
                RetailerId = dao.RetailerId,
                RecieptDate = dao.RecieptDate
            };
            await dal.Insert(dto);
            dao.RecieptId = dto.RecieptId;
        }
    }
}
