﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Estore.Management.DataAccess.Brands;
using Estore.Management.DataAccess.CustomerBills;

namespace Estore.Management.Library.CustomerBills
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CustomerBill : ICustomerBill
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBillDal CustomerBillDal; 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billOrderDal"></param>
        public CustomerBill()
        {
            this.CustomerBillDal = new CustomerBillDal(); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDate"></param>
        /// <returns></returns>
        public async Task<List<CustomerBillDao>> FetchList(string invoiceDate)
        {
            var list = new List<CustomerBillDao>();
            var dtos = await CustomerBillDal.FetchList(invoiceDate);
            dtos.ForEach(x => list.Add(new CustomerBillDao()
            {
                Address = x.Address,
                BaseAmount = x.BaseAmount,
                BatteryNumber = x.BatteryNumber,
                BillDateTime = x.BillDateTime,
                CGST = x.CGST,
                ChargerNumber = x.ChargerNumber,
                Discount = x.Discount,
                ErrorMessage = string.Empty,
                GrandTotal = x.GrandTotal,
                Id = x.Id,
                IMEINumber1 = x.IMEINumber1,
                IMEINumber2 = x.IMEINumber2,
                InvoiceDate = x.InvoiceDate,
                InvoiceNumber = x.InvoiceNumber,
                MobileNumber = x.MobileNumber,
                ModelNumber = x.ModelNumber,
                Name = x.Name,
                SGST = x.SGST,
                Status = true,
                CatelogId = x.CatelogId,
                CatelogDisplayName = x.CatelogId != null ? BrandDal.GetName(x.BrandTypeId.Value) + " "  + x.CatelogDisplayName : ""
            }));
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<CustomerBillDao> Fetch(decimal invoiceId)
        {
            var customerBill = await CustomerBillDal.Fetch(invoiceId);
            return new CustomerBillDao()
            {
                Address = customerBill.Address,
                BaseAmount = customerBill.BaseAmount,
                BatteryNumber = customerBill.BatteryNumber,
                BillDateTime = customerBill.BillDateTime,
                CGST = customerBill.CGST,
                ChargerNumber = customerBill.ChargerNumber,
                Discount = customerBill.Discount,
                ErrorMessage = string.Empty,
                GrandTotal = customerBill.GrandTotal,
                Id = customerBill.Id,
                IMEINumber1 = customerBill.IMEINumber1,
                IMEINumber2 = customerBill.IMEINumber2,
                InvoiceDate = customerBill.InvoiceDate,
                InvoiceNumber = customerBill.InvoiceNumber,
                MobileNumber = customerBill.MobileNumber,
                ModelNumber = customerBill.ModelNumber,
                Name = customerBill.Name,
                SGST = customerBill.SGST,
                Status = true,
                CatelogId = customerBill.CatelogId,
                CatelogDisplayName = customerBill.CatelogId != null ? DataAccess.Brands.BrandDal.GetName(customerBill.BrandTypeId.Value) + " " + customerBill.CatelogDisplayName : ""
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        public async Task New(CustomerBillDao dao)
        {
            if(string.IsNullOrEmpty(dao.InvoiceNumber) || string.IsNullOrEmpty(dao.Address) || string.IsNullOrEmpty(dao.Name) || string.IsNullOrEmpty(dao.IMEINumber1) || string.IsNullOrEmpty(dao.MobileNumber) || dao.GrandTotal == 0 || dao.CatelogId == null || dao.CatelogId < 1)
            {
                dao.Status = false;
                dao.ErrorMessage = "Please fill all mandatory fields.";
                return;
            }

            var dto = new CustomerBillDto()
            {
                Address = dao.Address,
                BaseAmount = dao.BaseAmount,
                BatteryNumber = dao.BatteryNumber,
                BillDateTime = dao.BillDateTime,
                CGST = dao.CGST,
                ChargerNumber = dao.ChargerNumber,
                Discount = dao.Discount,
                GrandTotal = dao.GrandTotal,
                Id = dao.Id,
                IMEINumber1 = dao.IMEINumber1,
                IMEINumber2 = dao.IMEINumber2,
                InvoiceDate = dao.InvoiceDate,
                InvoiceNumber = dao.InvoiceNumber,
                MobileNumber = dao.MobileNumber,
                ModelNumber = dao.ModelNumber,
                Name = dao.Name,
                SGST = dao.SGST,
                CatelogId = dao.CatelogId
            };
            await CustomerBillDal.Insert(dto);
            dao.Id = dto.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBillId"></param>
        /// <returns></returns>
        public async Task Delete(decimal customerBillId)
        {
            await CustomerBillDal.Delete(customerBillId);
        }
    }
}
