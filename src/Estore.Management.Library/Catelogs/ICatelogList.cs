﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICatelogList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandTypeId"></param>
        /// <returns></returns>
        Task<List<CatelogInfoDao>> Get(int? brandTypeId = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        Task<List<SelectedDateCatelogInfoDao>> Get(DateTime dateTime, decimal catelogId);
    }
}
