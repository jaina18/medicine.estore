﻿using Estore.Management.DataAccess.Catelogs;
using System.Threading.Tasks;

namespace Estore.Management.Library.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Catelog : ICatelog
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelogDal dal;

        /// <summary>
        /// 
        /// </summary>
        public Catelog()
        {
            this.dal = new CatelogDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task<CatelogDao> Get(decimal catelogId)
        {
            var dto = await dal.Get(catelogId);
            return new CatelogDao()
            {
                RAM = dto.RAM,
                Name = dto.Name,
                Memory = dto.Memory,
                Active = dto.Active,
                BrandTypeId = dto.BrandTypeId,
                CatelogId = dto.CatelogId,
                GSTPercentage = dto.GSTPercentage
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(CatelogDao dao)
        {
            var dto = new CatelogDto()
            {
                BrandTypeId = dao.BrandTypeId,
                CatelogId = dao.CatelogId,
                CreatedDate = dao.CreatedDate,
                Memory = dao.Memory,
                Name = dao.Name,
                RAM = dao.RAM,
                Active = dao.Active,
                GSTPercentage = dao.GSTPercentage
            };

            await dal.Insert(dto);
            dao.CatelogId = dao.CatelogId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Save(CatelogDao dao)
        {
            var dto = new CatelogDto()
            {
                CatelogId = dao.CatelogId,
                BrandTypeId = dao.BrandTypeId,
                Memory = dao.Memory,
                Name = dao.Name,
                RAM = dao.RAM,
                GSTPercentage = dao.GSTPercentage
            };
            await dal.Update(dto);
            dao.CatelogId = dao.CatelogId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task Status(decimal catelogId)
        {
            await dal.UpdateStatus(catelogId);
        }
    }
}
