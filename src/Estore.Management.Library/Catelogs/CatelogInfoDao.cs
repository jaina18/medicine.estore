﻿using System;

namespace Estore.Management.Library.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CatelogInfoDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int BrandTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RAM { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Memory { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastOrderDateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int LastOrderQuantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int GSTPercentage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get { return this.BrandName + " " + this.Name + " - " + this.GSTPercentage; } }
    }
}
