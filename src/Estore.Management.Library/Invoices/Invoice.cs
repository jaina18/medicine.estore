﻿using Estore.Management.DataAccess.Invoices;
using Estore.Management.DataAccess.InvoiceLines;
using System.Threading.Tasks;
using System.Linq;
using Estore.Management.DataAccess.Brands;

namespace Estore.Management.Library.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Invoice : IInvoice
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoiceDal dal;

        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoiceLineDal invoiceLineDal;

        /// <summary>
        /// 
        /// </summary>
        public Invoice()
        {
            this.dal = new InvoiceDal();
            this.invoiceLineDal = new InvoiceLineDal();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<InvoiceInfoDao> Get(decimal invoiceId)
        {
            var dto = await dal.Get(invoiceId);
            var dao = new InvoiceInfoDao()
            {
                Tax = dto.Tax,
                Amount = dto.Amount,
                InvoiceDate = dto.InvoiceDate.ToString("dd-MMM-yyyy"),
                InvoiceId = dto.InvoiceId,
                InvoiceNumber = dto.InvoiceNumber,
                RetailerId = dto.RetailerId,
                Lines = dto.Lines.Select(line => new InvoiceLineInfoDao()
                {
                    BrandName = BrandDal.GetName(line.BrandTypeId),
                    CatelogId = line.CatelogId,
                    CatelogName = line.CatelogName,
                    LineId = line.LineId,
                    LineNumber = line.LineNumber,
                    Price = line.Price,
                    Quantity = line.Quantity,
                    Tax = line.Tax
                }).ToList()
            };
            return dao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Delete(InvoiceDao dao)
        {
            var dto = new InvoiceDto()
            {
                InvoiceId = dao.InvoiceId,
                Archived = true
            };
            await dal.Update(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(InvoiceDao dao)
        {
            if (string.IsNullOrEmpty(dao.InvoiceNumber))
            {
                dao.Status = false;
                dao.ErrorMessage = "Please fill all mandatory fields.";
                return;
            }
            if (dao.InvoiceDate == null)
            {
                dao.Status = false;
                dao.ErrorMessage = "Please fill all mandatory fields.";
                return;
            }
            var calculatedAmount = dao.InvoiceLines.Where(x => x.Archived).Sum(x => (x.Quantity * x.Price) + x.Tax);
            var dto = new InvoiceDto()
            {
                Amount = calculatedAmount,
                InvoiceNumber = dao.InvoiceNumber,
                RetailerId = dao.RetailerId,
                InvoiceDate = dao.InvoiceDate.Value,
                Tax = dao.Tax
            };
            await dal.Insert(dto);
            dao.InvoiceId = dto.InvoiceId;
            if (dao.InvoiceLines != null && dao.InvoiceLines.Count() > 0)
            {
                await AddInvoiceLines(dao);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        private async Task AddInvoiceLines(InvoiceDao dao)
        {
            await Task.FromResult(1);
            if (dao.InvoiceLines.Count > 0)
            {
                dao.InvoiceLines.ForEach(async item =>
                {
                    if (item.Archived)
                    {
                        var dto = new InvoiceLineDto()
                        {
                            Archived = false,
                            CatelogId = item.CatelogId,
                            InvoiceId = dao.InvoiceId,
                            LineNumber = item.LineNumber,
                            Price = item.Price,
                            Quantity = item.Quantity,
                            Tax = item.Tax
                        };
                        await invoiceLineDal.Insert(dto);
                        item.LineId = dto.LineId;
                    }
                });
            }
        }
    }
}
