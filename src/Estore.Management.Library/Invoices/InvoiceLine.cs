﻿using Estore.Management.DataAccess.InvoiceLines;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceLine : IInvoiceLine
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly InvoiceLineDal dal;

        /// <summary>
        /// 
        /// </summary>
        public InvoiceLine()
        {
            this.dal = new InvoiceLineDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<List<InvoiceLineDao>> Get(decimal invoiceId)
        {
            var list = new List<InvoiceLineDao>();
            var dtos = await dal.FetchList(invoiceId);
            dtos.ForEach(item => new InvoiceLineDao()
            {
                Archived = item.Archived ?? false,
                CatelogId = item.CatelogId,
                InvoiceId = item.InvoiceId,
                LineId = item.LineId,
                LineNumber = item.LineNumber,
                Price = item.Price,
                Quantity = item.Quantity
            });
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task New(InvoiceLineDao dao)
        {
            var dto = new InvoiceLineDto()
            {
                LineId = dao.LineId,
                Archived = dao.Archived,
                CatelogId = dao.CatelogId,
                InvoiceId = dao.InvoiceId,
                LineNumber = dao.LineNumber,
                Price = dao.Price,
                Quantity = dao.Quantity
            };
            await dal.Insert(dto);
        }
    }
}
