﻿using System;

namespace Estore.Management.Library.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceLineInfoDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal LineId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int LineNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CatelogName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Tax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }
    }
}
