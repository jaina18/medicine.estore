﻿using System;

namespace Estore.Management.Library.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceLineDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal LineId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int LineNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal InvoiceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Tax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }
    }
}
