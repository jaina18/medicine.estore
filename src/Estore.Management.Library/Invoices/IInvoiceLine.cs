﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.Library.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IInvoiceLine
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        Task<List<InvoiceLineDao>> Get(decimal invoiceId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task New(InvoiceLineDao dao);
    }
}
