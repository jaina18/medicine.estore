﻿using Estore.Management.Library.Shared;
using System.Collections.Generic;

namespace Estore.Management.Library.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceInfoDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal InvoiceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Tax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InvoiceDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<InvoiceLineInfoDao> Lines { get; set; }
    }
}
