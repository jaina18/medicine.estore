﻿using System.Threading.Tasks;

namespace Estore.Management.Library.Users
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<UserDao> Get(string emailAddress, string password);
    }
}
