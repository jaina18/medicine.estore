﻿using Estore.Management.DataAccess.Reports;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DailyReciept : IReport<List<DailyRecieptDao>, DailyRecieptCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDailyRecieptDal dal;

        /// <summary>
        /// 
        /// </summary>
        public DailyReciept()
        {
            this.dal = new DailyRecieptDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public async Task<List<DailyRecieptDao>> Get(DailyRecieptCriteria criteria)
        {
            var dtos = await dal.FetchList(criteria.DateTime);
            var list = new List<DailyRecieptDao>();
            dtos.ForEach(item => list.Add(new DailyRecieptDao()
            {
                Active = true,
                Amount = item.Amount,
                RecieptId = item.RecieptId,
                RecieptNumber = item.RecieptNumber,
                RetailerId = item.RetailerId,
                RetailerName = item.RetailerName
            }));
            return list;
        }
    }
}
