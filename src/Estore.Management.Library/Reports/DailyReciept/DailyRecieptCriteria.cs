﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DailyRecieptCriteria
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        public DailyRecieptCriteria(DateTime dateTime)
        {
            DateTime = dateTime;
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DateTime { get; set; }
    }
}
