﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReport<T1, T2>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        Task<T1> Get(T2 criteria);
    }
}
