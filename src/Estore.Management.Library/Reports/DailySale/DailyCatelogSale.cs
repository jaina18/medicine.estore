﻿using Estore.Management.DataAccess.Reports;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Estore.Management.DataAccess.Brands;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DailyCatelogSale : IReport<List<DailyCatelogSaleDao>, DailyCatelogSaleCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDailyCatelogSaleDal dal;

        /// <summary>
        /// 
        /// </summary>
        public DailyCatelogSale()
        {
            this.dal = new DailyCatelogSaleDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<DailyCatelogSaleDao>> Get(DailyCatelogSaleCriteria criteria)
        {
            var dtos = await dal.FetchList(criteria.DateTime);
            var list = new List<DailyCatelogSaleDao>();
            dtos.ForEach(item => list.Add(new DailyCatelogSaleDao()
            {
                BrandName = BrandDal.GetName(item.BrandTypeId),
                CatelogId = item.CatelogId,
                CatelogName = item.CatelogName,
                Quantity = item.Quantity
            }));
            return list;
        }
    }
}
