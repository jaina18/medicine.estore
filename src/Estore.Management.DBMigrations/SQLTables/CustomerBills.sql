/****** Object:  Table [dbo].[CustomerBills]    Script Date: 03-10-2021 14:47:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerBills](
	[Id] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CatelogId] [numeric](18, 0) NULL,
	[InvoiceNumber] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](500) NOT NULL,
	[MobileNumber] [nvarchar](10) NOT NULL,
	[ModelNumber] [nvarchar](500) NOT NULL,
	[IMEINumber1] [nvarchar](100) NOT NULL,
	[IMEINumber2] [nvarchar](100) NOT NULL,
	[BatteryNumber] [nvarchar](100) NULL,
	[ChargerNumber] [nvarchar](100) NOT NULL,
	[BaseAmount] [decimal](18, 0) NOT NULL,
	[Discount] [decimal](18, 0) NULL,
	[SGST] [decimal](18, 0) NOT NULL,
	[CGST] [decimal](18, 0) NOT NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceDateTime] [nvarchar](50) NOT NULL,
	[BillDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_OrderBills] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CustomerBills]  WITH CHECK ADD  CONSTRAINT [FK_CustomerBills_Catelogs] FOREIGN KEY([CatelogId])
REFERENCES [dbo].[Catelogs] ([CatelogId])
GO

ALTER TABLE [dbo].[CustomerBills] CHECK CONSTRAINT [FK_CustomerBills_Catelogs]
GO


