/****** Object:  Table [dbo].[InvoiceLines]    Script Date: 03-10-2021 14:48:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InvoiceLines](
	[LineId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[InvoiceId] [numeric](18, 0) NOT NULL,
	[CatelogId] [numeric](18, 0) NOT NULL,
	[Price] [numeric](10, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Archived] [bit] NULL,
 CONSTRAINT [PK_InvoiceLines] PRIMARY KEY CLUSTERED 
(
	[LineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[InvoiceLines]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceLines_Catelogs] FOREIGN KEY([CatelogId])
REFERENCES [dbo].[Catelogs] ([CatelogId])
GO

ALTER TABLE [dbo].[InvoiceLines] CHECK CONSTRAINT [FK_InvoiceLines_Catelogs]
GO

ALTER TABLE [dbo].[InvoiceLines]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceLines_Invoices] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoices] ([InvoiceId])
GO

ALTER TABLE [dbo].[InvoiceLines] CHECK CONSTRAINT [FK_InvoiceLines_Invoices]
GO


