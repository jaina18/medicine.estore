/****** Object:  Table [dbo].[Catelogs]    Script Date: 10-10-2021 02:11:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Catelogs](
	[CatelogId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BrandTypeId] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[RAM] [varchar](50) NOT NULL,
	[Memory] [varchar](50) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NULL,
	[GSTPercentage] [int] NULL,
 CONSTRAINT [PK_Catelogs] PRIMARY KEY CLUSTERED 
(
	[CatelogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


